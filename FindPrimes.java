import java.util.Scanner;
 
public class FindPrimes {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        int counter;
        
        System.out.println("Give me a number: ");
        int number = scan.nextInt();
        
        while(number > 2) {
            
            counter = 0;
            for(int i = 2; i < number; i++)
            {
                if(number % i == 0) {
                    counter++;
                }
            }
            if(counter == 0) {
                System.out.println(number);
            }
            number--;
        }
    }
}